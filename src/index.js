import { Controller } from "stimulus";
import cleanArray from "clean-array";

/**
 * Stimulus controller to fetch data from https://getaddress.io and feed into
 * a form
 * @extends Controller
 */
export class GetAddressIOController extends Controller {
  static targets = [
    "postcode",
    "number",
    "list",
    "line1",
    "line2",
    "line3",
    "line4",
    "locality",
    "townOrCity",
    "county",
    "listContainer",
    "list",
    "fields",
    "errorMessage",
    "latitude",
    "longitude",
  ];

  connect() {
    this.listContainerTarget.hidden = true;

    if (this.postcodeTarget.value === "") this.fieldsTarget.hidden = true;
  }

  search(e) {
    e.preventDefault();

    if (!this.postcodeTarget.value.length) return;

    fetch(this.url)
      .then((response) => {
        if (response.status >= 200 && response.status <= 300) {
          return response;
        } else {
          let error = new Error(response.statusText);
          error.response = response;
          throw error;
        }
      })
      .then((response) => response.json())
      .then((json) => {
        this.coordinates = {
          latitude: json.latitude,
          longitude: json.longitude,
        };
        this.options = json.addresses;
      })
      .catch((error) => {
        console.error(error);
        this.listContainerTarget.hidden = true;
        this.fieldsTarget.hidden = false;
        this.errorMessageTarget.textContent =
          "There was an error finding your postcode. Please enter your address manually below.";
      });
  }

  fill(e) {
    let address =
      this.listTarget.options[this.listTarget.selectedIndex].dataset.address;
    if (!address) return;

    this.listContainerTarget.hidden = true;
    this.address = JSON.parse(address);
  }

  set address(address) {
    this.line1Target.value = address.line_1;
    this.line2Target.value = address.line_2;
    this.townOrCityTarget.value = address.town_or_city;

    if (this.hasline3Target) {
      this.line3Target.value = address.line_3;
    }
    if (this.hasline4Target) {
      this.line4Target.value = address.line_4;
    }
    if (this.haslocalityTarget) {
      this.localityTarget.value = address.locality;
    }
    if (this.hasCountyTarget) {
      this.countyTarget.value = address.county;
    }
    if (this.hasLatitudeTarget) {
      this.latitudeTarget.value = this.coordinates.latitude;
    }
    if (this.hasLongitudeTarget) {
      this.longitudeTarget.value = this.coordinates.longitude;
    }

    this.fieldsTarget.hidden = false;
  }

  set options(addresses) {
    this.listTarget.innerHTML = "";
    this.listTarget.appendChild(new Option("Select your address"));
    for (const address of addresses) {
      let cleanAddress = cleanArray(address.formatted_address);
      let option = new Option(cleanAddress.join(", "));
      option.dataset.address = JSON.stringify(address);
      this.listTarget.appendChild(option);
    }
    this.listContainerTarget.hidden = false;
    this.fieldsTarget.hidden = true;
    this.errorMessageTarget.textContent = "";
  }

  get postcode() {
    return this.postcodeTarget.value;
  }

  get apiKey() {
    return document
      .querySelector("meta[name='getaddress_api_key']")
      .getAttribute("content");
  }

  get url() {
    return `https://api.getaddress.io/find/${this.postcode}?api-key=${this.apiKey}&expand=true&sort=true`;
  }
}
