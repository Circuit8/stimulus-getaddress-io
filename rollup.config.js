import resolve from "@rollup/plugin-node-resolve";
import { terser } from "rollup-plugin-terser";

const terserOptions = {
  mangle: false,
  compress: false,
  format: {
    beautify: true,
    indent_level: 2,
  },
};

export default [
  {
    input: "src/index.js",
    output: [
      {
        file: "dist/stimulus-getaddress-io.js",
        format: "umd",
        name: "GetAddressIOController",
      },

      {
        file: "dist/stimulus-getaddress-io.esm.js",
        format: "es",
      },
    ],
    plugins: [resolve(), terser(terserOptions)],
  },
];
