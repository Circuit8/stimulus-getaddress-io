(function(global, factory) {
  typeof exports === "object" && typeof module !== "undefined" ? factory(exports, require("clean-array")) : typeof define === "function" && define.amd ? define([ "exports", "clean-array" ], factory) : (global = typeof globalThis !== "undefined" ? globalThis : global || self, 
  factory(global.GetAddressIOController = {}, global.cleanArray));
})(this, (function(exports, cleanArray) {
  "use strict";
  function _interopDefaultLegacy(e) {
    return e && typeof e === "object" && "default" in e ? e : {
      default: e
    };
  }
  var cleanArray__default = _interopDefaultLegacy(cleanArray);
  function camelize(value) {
    return value.replace(/(?:[_-])([a-z0-9])/g, ((_, char) => char.toUpperCase()));
  }
  function capitalize(value) {
    return value.charAt(0).toUpperCase() + value.slice(1);
  }
  function dasherize(value) {
    return value.replace(/([A-Z])/g, ((_, char) => `-${char.toLowerCase()}`));
  }
  function readInheritableStaticArrayValues(constructor, propertyName) {
    const ancestors = getAncestorsForConstructor(constructor);
    return Array.from(ancestors.reduce(((values, constructor) => {
      getOwnStaticArrayValues(constructor, propertyName).forEach((name => values.add(name)));
      return values;
    }), new Set));
  }
  function readInheritableStaticObjectPairs(constructor, propertyName) {
    const ancestors = getAncestorsForConstructor(constructor);
    return ancestors.reduce(((pairs, constructor) => {
      pairs.push(...getOwnStaticObjectPairs(constructor, propertyName));
      return pairs;
    }), []);
  }
  function getAncestorsForConstructor(constructor) {
    const ancestors = [];
    while (constructor) {
      ancestors.push(constructor);
      constructor = Object.getPrototypeOf(constructor);
    }
    return ancestors.reverse();
  }
  function getOwnStaticArrayValues(constructor, propertyName) {
    const definition = constructor[propertyName];
    return Array.isArray(definition) ? definition : [];
  }
  function getOwnStaticObjectPairs(constructor, propertyName) {
    const definition = constructor[propertyName];
    return definition ? Object.keys(definition).map((key => [ key, definition[key] ])) : [];
  }
  (() => {
    function extendWithReflect(constructor) {
      function extended() {
        return Reflect.construct(constructor, arguments, new.target);
      }
      extended.prototype = Object.create(constructor.prototype, {
        constructor: {
          value: extended
        }
      });
      Reflect.setPrototypeOf(extended, constructor);
      return extended;
    }
    function testReflectExtension() {
      const a = function() {
        this.a.call(this);
      };
      const b = extendWithReflect(a);
      b.prototype.a = function() {};
      return new b;
    }
    try {
      testReflectExtension();
      return extendWithReflect;
    } catch (error) {
      return constructor => class extended extends constructor {};
    }
  })();
  function ClassPropertiesBlessing(constructor) {
    const classes = readInheritableStaticArrayValues(constructor, "classes");
    return classes.reduce(((properties, classDefinition) => Object.assign(properties, propertiesForClassDefinition(classDefinition))), {});
  }
  function propertiesForClassDefinition(key) {
    return {
      [`${key}Class`]: {
        get() {
          const {classes: classes} = this;
          if (classes.has(key)) {
            return classes.get(key);
          } else {
            const attribute = classes.getAttributeName(key);
            throw new Error(`Missing attribute "${attribute}"`);
          }
        }
      },
      [`${key}Classes`]: {
        get() {
          return this.classes.getAll(key);
        }
      },
      [`has${capitalize(key)}Class`]: {
        get() {
          return this.classes.has(key);
        }
      }
    };
  }
  function TargetPropertiesBlessing(constructor) {
    const targets = readInheritableStaticArrayValues(constructor, "targets");
    return targets.reduce(((properties, targetDefinition) => Object.assign(properties, propertiesForTargetDefinition(targetDefinition))), {});
  }
  function propertiesForTargetDefinition(name) {
    return {
      [`${name}Target`]: {
        get() {
          const target = this.targets.find(name);
          if (target) {
            return target;
          } else {
            throw new Error(`Missing target element "${name}" for "${this.identifier}" controller`);
          }
        }
      },
      [`${name}Targets`]: {
        get() {
          return this.targets.findAll(name);
        }
      },
      [`has${capitalize(name)}Target`]: {
        get() {
          return this.targets.has(name);
        }
      }
    };
  }
  function ValuePropertiesBlessing(constructor) {
    const valueDefinitionPairs = readInheritableStaticObjectPairs(constructor, "values");
    const propertyDescriptorMap = {
      valueDescriptorMap: {
        get() {
          return valueDefinitionPairs.reduce(((result, valueDefinitionPair) => {
            const valueDescriptor = parseValueDefinitionPair(valueDefinitionPair);
            const attributeName = this.data.getAttributeNameForKey(valueDescriptor.key);
            return Object.assign(result, {
              [attributeName]: valueDescriptor
            });
          }), {});
        }
      }
    };
    return valueDefinitionPairs.reduce(((properties, valueDefinitionPair) => Object.assign(properties, propertiesForValueDefinitionPair(valueDefinitionPair))), propertyDescriptorMap);
  }
  function propertiesForValueDefinitionPair(valueDefinitionPair) {
    const definition = parseValueDefinitionPair(valueDefinitionPair);
    const {key: key, name: name, reader: read, writer: write} = definition;
    return {
      [name]: {
        get() {
          const value = this.data.get(key);
          if (value !== null) {
            return read(value);
          } else {
            return definition.defaultValue;
          }
        },
        set(value) {
          if (value === undefined) {
            this.data.delete(key);
          } else {
            this.data.set(key, write(value));
          }
        }
      },
      [`has${capitalize(name)}`]: {
        get() {
          return this.data.has(key) || definition.hasCustomDefaultValue;
        }
      }
    };
  }
  function parseValueDefinitionPair([token, typeDefinition]) {
    return valueDescriptorForTokenAndTypeDefinition(token, typeDefinition);
  }
  function parseValueTypeConstant(constant) {
    switch (constant) {
     case Array:
      return "array";

     case Boolean:
      return "boolean";

     case Number:
      return "number";

     case Object:
      return "object";

     case String:
      return "string";
    }
  }
  function parseValueTypeDefault(defaultValue) {
    switch (typeof defaultValue) {
     case "boolean":
      return "boolean";

     case "number":
      return "number";

     case "string":
      return "string";
    }
    if (Array.isArray(defaultValue)) return "array";
    if (Object.prototype.toString.call(defaultValue) === "[object Object]") return "object";
  }
  function parseValueTypeObject(typeObject) {
    const typeFromObject = parseValueTypeConstant(typeObject.type);
    if (typeFromObject) {
      const defaultValueType = parseValueTypeDefault(typeObject.default);
      if (typeFromObject !== defaultValueType) {
        throw new Error(`Type "${typeFromObject}" must match the type of the default value. Given default value: "${typeObject.default}" as "${defaultValueType}"`);
      }
      return typeFromObject;
    }
  }
  function parseValueTypeDefinition(typeDefinition) {
    const typeFromObject = parseValueTypeObject(typeDefinition);
    const typeFromDefaultValue = parseValueTypeDefault(typeDefinition);
    const typeFromConstant = parseValueTypeConstant(typeDefinition);
    const type = typeFromObject || typeFromDefaultValue || typeFromConstant;
    if (type) return type;
    throw new Error(`Unknown value type "${typeDefinition}"`);
  }
  function defaultValueForDefinition(typeDefinition) {
    const constant = parseValueTypeConstant(typeDefinition);
    if (constant) return defaultValuesByType[constant];
    const defaultValue = typeDefinition.default;
    if (defaultValue !== undefined) return defaultValue;
    return typeDefinition;
  }
  function valueDescriptorForTokenAndTypeDefinition(token, typeDefinition) {
    const key = `${dasherize(token)}-value`;
    const type = parseValueTypeDefinition(typeDefinition);
    return {
      type: type,
      key: key,
      name: camelize(key),
      get defaultValue() {
        return defaultValueForDefinition(typeDefinition);
      },
      get hasCustomDefaultValue() {
        return parseValueTypeDefault(typeDefinition) !== undefined;
      },
      reader: readers[type],
      writer: writers[type] || writers.default
    };
  }
  const defaultValuesByType = {
    get array() {
      return [];
    },
    boolean: false,
    number: 0,
    get object() {
      return {};
    },
    string: ""
  };
  const readers = {
    array(value) {
      const array = JSON.parse(value);
      if (!Array.isArray(array)) {
        throw new TypeError("Expected array");
      }
      return array;
    },
    boolean(value) {
      return !(value == "0" || value == "false");
    },
    number(value) {
      return Number(value);
    },
    object(value) {
      const object = JSON.parse(value);
      if (object === null || typeof object != "object" || Array.isArray(object)) {
        throw new TypeError("Expected object");
      }
      return object;
    },
    string(value) {
      return value;
    }
  };
  const writers = {
    default: writeString,
    array: writeJSON,
    object: writeJSON
  };
  function writeJSON(value) {
    return JSON.stringify(value);
  }
  function writeString(value) {
    return `${value}`;
  }
  class Controller {
    constructor(context) {
      this.context = context;
    }
    static get shouldLoad() {
      return true;
    }
    get application() {
      return this.context.application;
    }
    get scope() {
      return this.context.scope;
    }
    get element() {
      return this.scope.element;
    }
    get identifier() {
      return this.scope.identifier;
    }
    get targets() {
      return this.scope.targets;
    }
    get classes() {
      return this.scope.classes;
    }
    get data() {
      return this.scope.data;
    }
    initialize() {}
    connect() {}
    disconnect() {}
    dispatch(eventName, {target: target = this.element, detail: detail = {}, prefix: prefix = this.identifier, bubbles: bubbles = true, cancelable: cancelable = true} = {}) {
      const type = prefix ? `${prefix}:${eventName}` : eventName;
      const event = new CustomEvent(type, {
        detail: detail,
        bubbles: bubbles,
        cancelable: cancelable
      });
      target.dispatchEvent(event);
      return event;
    }
  }
  Controller.blessings = [ ClassPropertiesBlessing, TargetPropertiesBlessing, ValuePropertiesBlessing ];
  Controller.targets = [];
  Controller.values = {};
  class GetAddressIOController extends Controller {
    static targets=[ "postcode", "number", "list", "line1", "line2", "line3", "line4", "locality", "townOrCity", "county", "listContainer", "list", "fields", "errorMessage", "latitude", "longitude" ];
    connect() {
      this.listContainerTarget.hidden = true;
      if (this.postcodeTarget.value === "") this.fieldsTarget.hidden = true;
    }
    search(e) {
      e.preventDefault();
      if (!this.postcodeTarget.value.length) return;
      fetch(this.url).then((response => {
        if (response.status >= 200 && response.status <= 300) {
          return response;
        } else {
          let error = new Error(response.statusText);
          error.response = response;
          throw error;
        }
      })).then((response => response.json())).then((json => {
        this.coordinates = {
          latitude: json.latitude,
          longitude: json.longitude
        };
        this.options = json.addresses;
      })).catch((error => {
        console.error(error);
        this.listContainerTarget.hidden = true;
        this.fieldsTarget.hidden = false;
        this.errorMessageTarget.textContent = "There was an error finding your postcode. Please enter your address manually below.";
      }));
    }
    fill(e) {
      let address = this.listTarget.options[this.listTarget.selectedIndex].dataset.address;
      if (!address) return;
      this.listContainerTarget.hidden = true;
      this.address = JSON.parse(address);
    }
    set address(address) {
      this.line1Target.value = address.line_1;
      this.line2Target.value = address.line_2;
      this.townOrCityTarget.value = address.town_or_city;
      if (this.hasline3Target) {
        this.line3Target.value = address.line_3;
      }
      if (this.hasline4Target) {
        this.line4Target.value = address.line_4;
      }
      if (this.haslocalityTarget) {
        this.localityTarget.value = address.locality;
      }
      if (this.hasCountyTarget) {
        this.countyTarget.value = address.county;
      }
      if (this.hasLatitudeTarget) {
        this.latitudeTarget.value = this.coordinates.latitude;
      }
      if (this.hasLongitudeTarget) {
        this.longitudeTarget.value = this.coordinates.longitude;
      }
      this.fieldsTarget.hidden = false;
    }
    set options(addresses) {
      this.listTarget.innerHTML = "";
      this.listTarget.appendChild(new Option("Select your address"));
      for (const address of addresses) {
        let cleanAddress = cleanArray__default["default"](address.formatted_address);
        let option = new Option(cleanAddress.join(", "));
        option.dataset.address = JSON.stringify(address);
        this.listTarget.appendChild(option);
      }
      this.listContainerTarget.hidden = false;
      this.fieldsTarget.hidden = true;
      this.errorMessageTarget.textContent = "";
    }
    get postcode() {
      return this.postcodeTarget.value;
    }
    get apiKey() {
      return document.querySelector("meta[name='getaddress_api_key']").getAttribute("content");
    }
    get url() {
      return `https://api.getaddress.io/find/${this.postcode}?api-key=${this.apiKey}&expand=true&sort=true`;
    }
  }
  exports.GetAddressIOController = GetAddressIOController;
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
}));
