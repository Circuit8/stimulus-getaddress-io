# Stimulus Getaddress.io Controller

Stimulus controller to provide data into forms using getaddress.io

## Installation

```shell
$ yarn add stimulus-getaddress-io
```

## Usage

Register the controller with Stimulus:

```javascript
// application.js
import { Application } from 'stimulus';
import { GetAddressIOController } from 'stimulus-getaddress-io'

const application = Application.start()
application.register('getaddress-io', GetAddressIOController)
```

## Contributing

Fork the project.

Install dependencies
```shell
$ yarn install
```

Start the test watcher
```shell
$ yarn test:watch
```

Running one-off test runs can be done with:
```shell
$ yarn test
```

Write some tests, and add your feature. Send a PR.
